#pragma once

#include <vector>

#include "math_util.h"

struct Sprite{
	unsigned int textureID = 0;
	float rotation = 0.0f;
	vec2 position = vec2(0.0f, 0.0f);
	vec2 scale = vec2(1.0f, 1.0f);
	vec4 color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
};

struct AnimatedSprite{
	unsigned int currentFrame = 0;
	unsigned int totalFrames = 0;
	float currentFrameTime = 0.0f;
	float rotation = 0.0f;
	vec2 position = vec2(0.0f, 0.0f);
	vec2 scale = vec2(1.0f, 1.0f);
	vec4 color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	std::vector<unsigned int> keyFrames;
	std::vector<float> frameDurations;
};

class SpriteRenderer{
public:
	vec2 cameraPosition = vec2(0, 0);
	vec2 cameraOrientation = vec2(0, 0);

	static SpriteRenderer* getInstance(); 
	~SpriteRenderer();
	
	void shutdown();
	void update();
	void prepare();
	void renderSprite(Sprite spt);
	void renderAnimatedSprite(AnimatedSprite* aspt, float delta);
	void setCameraProjection(float left, float right, float bottom, float top);
	void addKeyframeToAnimatedSprite(AnimatedSprite* aspt, unsigned int frame, float duration);
	void deleteImageData(unsigned int image);
	unsigned int loadImageData(unsigned char* data, int w, int h);
	
private:
	static SpriteRenderer* instance;
	
	void initialize();
	SpriteRenderer();
};
