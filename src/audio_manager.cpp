#include "audio_manager.h"

#include <vector>

#include <AL/al.h>
#include <AL/alc.h>

#include "io_manager.h"

AudioManager* AudioManager::instance = nullptr;

static const int MAX_AUDIO_SOURCES = 32;
static const int MAX_AUDIO_BUFFERS = 32;

ALCdevice *audioDevice;
ALCcontext *audioContext;

std::vector<unsigned int> audioSource;
std::vector<unsigned int> audioBuffers;

AudioManager::~AudioManager(){}

void AudioManager::shutdown(){
	if(instance){
		alDeleteSources(audioSource.size(), (ALuint*)&audioSource[0]);
		alDeleteBuffers(audioBuffers.size(), (ALuint*)&audioBuffers[0]);
	
		audioSource.clear();
		audioBuffers.clear();
		
		alcDestroyContext(audioContext);
		alcCloseDevice(audioDevice);
		
		delete instance;
		instance = nullptr;
	}
}

AudioManager* AudioManager::getInstance(){
	if(!instance){
		instance = new AudioManager();
	}
	
	return instance;
}	
	
AudioManager::AudioManager(){
	initialize();
}

void AudioManager::initialize(){
	audioSource.reserve(MAX_AUDIO_SOURCES);
	audioBuffers.reserve(MAX_AUDIO_BUFFERS);
	
	audioDevice = alcOpenDevice(NULL);
	if (!audioDevice){
		IOManager::getInstance()->displayMessageBox("OPENAL ERROR", "There was an error initializing OpenAL", 0);
	}
   
	
	audioContext = alcCreateContext(audioDevice, NULL);
	if (!alcMakeContextCurrent(audioContext)){
		IOManager::getInstance()->displayMessageBox("OPENAL ERROR", "There was an error creating OpenAL context", 0);
	}
}

void AudioManager::playSource(unsigned int source){
	alSourcePlay(source);
}

void AudioManager::stopSource(unsigned int source){
	alSourceStop(source);
}

void AudioManager::attachBufferToSource(unsigned int buffer, unsigned int source){
	alSourcei(source, AL_BUFFER, buffer);
}

void AudioManager::deleteSource(unsigned int source){
	unsigned int sz = audioSource.size();
	for(unsigned int i = 0; i < sz; i++){
		if(audioSource[i] == source){
			alDeleteSources(1, &source);
			audioSource.erase(audioSource.begin() + i);
			break;
		}
	}
}

void AudioManager::deleteBuffer(unsigned int buffer){
	unsigned int sz = audioBuffers.size();
	for(unsigned int i = 0; i < sz; i++){
		if(audioBuffers[i] == buffer){
			alDeleteBuffers(1, &buffer);
			audioBuffers.erase(audioBuffers.begin() + i);
			break;
		}
	}
} 

void AudioManager::setSourcePitch(unsigned int source, float pitch){
	alSourcef(source, AL_PITCH, pitch);
}

unsigned int AudioManager::createSource(){
	ALuint source;
	alGenSources(1, &source);
	alSourcef(source, AL_PITCH, 1);
	alSourcef(source, AL_GAIN, 1);
	alSource3f(source, AL_POSITION, 0, 0, 0);
	alSource3f(source, AL_VELOCITY, 0, 0, 0);
	alSourcei(source, AL_LOOPING, AL_FALSE);
	audioSource.push_back(source);
	return source;
}

unsigned int AudioManager::loadSoundData8M(unsigned char* data, unsigned int sizeInBytes, unsigned int sampleFreq){
	ALuint buffer;
	alGenBuffers(1, &buffer);
	alBufferData(buffer, AL_FORMAT_MONO8, data, sizeInBytes, sampleFreq);
	audioBuffers.push_back(buffer);
	
	return buffer;
}
