#include "render_manager.h"

#include "math_util.h"

RenderManager* RenderManager::instance = nullptr;

vec4 viewport = vec4(0, 0, 0, 0);
vec4 clearColor = vec4(0, 0, 0, 1);


RenderManager::~RenderManager(){
	
}

RenderManager::RenderManager(){
	initialize();
}

void RenderManager::shutdown(){
	if(instance){
		txtrdr->shutdown();
		sptrdr->shutdown();
		
		delete instance;
		instance = nullptr;
	}
}

void RenderManager::initialize(){
	glewInit();
}

void RenderManager::setViewport(int x, int y, int w, int h){
	viewport = vec4(x, y, w, h);
	glViewport(x, y, w, h);
}

void RenderManager::setClearColor(float r, float g, float b, float a){
	clearColor = vec4(r, g, b, a);
	glClearColor(r, g, b, a);
}

void RenderManager::clear(int field){
	glClear((GLbitfield)field);
}

RenderManager* RenderManager::getInstance(){
	if(!instance){
		instance = new RenderManager();
	}
	
	return instance;
}

SpriteRenderer* RenderManager::getSpriteRendererInstance(){
	sptrdr = SpriteRenderer::getInstance();
	sptrdr->setCameraProjection(viewport.x, viewport.z, viewport.y, viewport.w);
	return sptrdr;	
}

TextRenderer* RenderManager::getTextRendererInstance(){
	txtrdr = TextRenderer::getInstance();
	txtrdr->setProjection((float)viewport.x, (float)viewport.z, (float)viewport.y, (float)viewport.w);
	return txtrdr;
}
