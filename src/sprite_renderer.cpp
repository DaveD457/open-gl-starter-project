#include "sprite_renderer.h"

#include <vector>

#include <GL/glew.h>

#include "shader_compiler.h"

const unsigned short MAX_TEXTURE_SIZE = 1024;
const unsigned short MAX_TEXTURES = 128;


SpriteRenderer* SpriteRenderer::instance = nullptr;

const GLchar* vertexShaderSource = "#version 310 es\n"
    "layout (location = 0) in vec2 position;"
    "layout (location = 1) in vec2 texcoord;"
    "uniform mat4 mvpMatrix;"
    "out vec2 Texcoord;"
    "void main()"
    "{"   
    "    Texcoord = texcoord;"
    "    gl_Position = mvpMatrix * vec4(position, 0.0, 1.0);"
    "}";
const GLchar* fragmentShaderSource = "#version 310 es\n"
	"precision mediump float;"
	"precision mediump sampler2D;"
    "in vec2 Texcoord;"
    "out vec4 outColor;"
    "uniform vec4 spriteColor;"
    "uniform sampler2D tex;"
    "void main()"
    "{"
    "    outColor = spriteColor * texture(tex, Texcoord);//spriteColor;\n"
    "}";

GLuint spriteShader;
GLuint mvpMatID, spriteColorID;

GLuint spriteVao, spriteVbo, spriteEbo;
mat4 projectionMatrix = mat4(1);

GLuint defaultTexture;

std::vector<GLuint> textures;

SpriteRenderer::SpriteRenderer(){
	initialize();
}

SpriteRenderer::~SpriteRenderer(){
	
}

SpriteRenderer* SpriteRenderer::getInstance(){
	if(!instance){
		instance = new SpriteRenderer();
	}
	return instance;
}

void SpriteRenderer::shutdown(){
	if(instance){
		glDeleteVertexArrays(1, &spriteVao);
		glDeleteBuffers(1, &spriteVbo);
		glDeleteBuffers(1, &spriteEbo);
		glDeleteProgram(spriteShader);
		glDeleteTextures(textures.size(), &textures[0]);
		delete instance;
		instance = nullptr;
	}
}

void SpriteRenderer::update(){
	
}

void SpriteRenderer::prepare(){
	glUseProgram(spriteShader);	
	glBindVertexArray(spriteVao);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void SpriteRenderer::renderSprite(Sprite spt){
	glBindTexture(GL_TEXTURE_2D, spt.textureID);
	
	mat4 model = mat4(1);
	
	model = translate(model, vec3(spt.position, 0));
	model = rotate(model, spt.rotation, vec3(0, 0, 1));
	model = scale(model, vec3(spt.scale, 0));
	
	mat4 camera = mat4(1);
	camera = inverse(translate(camera, vec3(cameraPosition, 0)));
	mat4 mvp = projectionMatrix * camera * model;
	
	glUniform4fv(spriteColorID, 1, &spt.color[0]);
	glUniformMatrix4fv(mvpMatID, 1, GL_FALSE, value_ptr(mvp));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
}

void SpriteRenderer::renderAnimatedSprite(AnimatedSprite* aspt, float delta){
	unsigned int cfm = aspt->currentFrame;
	unsigned int max = aspt->totalFrames;
	float fmdur = aspt->frameDurations[cfm];
	float cftm = aspt->currentFrameTime + delta;
	if(cftm > fmdur){
		cfm = (cfm + 1) % max;
		cftm -= fmdur;
	}
	aspt->currentFrame = cfm;
	aspt->currentFrameTime = cftm;
	glBindTexture(GL_TEXTURE_2D, aspt->keyFrames[cfm]);
	
	mat4 model = mat4(1);
	
	model = translate(model, vec3(aspt->position, 0));
	model = rotate(model, aspt->rotation, vec3(0, 0, 1));
	model = scale(model, vec3(aspt->scale, 0));
	
	mat4 camera = mat4(1);
	camera = inverse(translate(camera, vec3(cameraPosition, 0)));
	mat4 mvp = projectionMatrix * camera * model;
	
	glUniform4fv(spriteColorID, 1, &aspt->color[0]);
	glUniformMatrix4fv(mvpMatID, 1, GL_FALSE, value_ptr(mvp));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
}

void SpriteRenderer::setCameraProjection(float left, float right, float bottom, float top){
	projectionMatrix = ortho(left, right, bottom, top);
}

void SpriteRenderer::addKeyframeToAnimatedSprite(AnimatedSprite* aspt, unsigned int frame, float duration){
	aspt->keyFrames.push_back(frame);
	aspt->frameDurations.push_back(duration);
	aspt->totalFrames++;
}

void SpriteRenderer::deleteImageData(unsigned int image){
	unsigned int sz = textures.size();
	for(unsigned int i = 0; i < sz; i++){
		if(image == textures[i]){
			glDeleteTextures(1, &image);
			textures.erase(textures.begin() + i);
		}
	}
}

unsigned int SpriteRenderer::loadImageData(unsigned char* data, int w, int h){
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	textures.push_back(tex);
	return tex;
}

void SpriteRenderer::initialize(){
	float vertexData[] = {
		-0.5f, -0.5f, 0.0f, 0.0f,  
        -0.5f,  0.5f, 0.0f, 1.0f, 
		 0.5f,  0.5f, 1.0f, 1.0f, 
		 0.5f, -0.5f, 1.0f, 0.0f  
	};
	
	unsigned char elementData[] = {
		0, 1, 2, 2, 3, 0
	};
	
	glGenVertexArrays(1, &spriteVao);
	glBindVertexArray(spriteVao);
	
	glGenBuffers(1, &spriteVbo);
	glBindBuffer(GL_ARRAY_BUFFER, spriteVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	
	glGenBuffers(1, &spriteEbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, spriteEbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementData), elementData, GL_STATIC_DRAW);
	
	spriteShader = compileShaderVF(vertexShaderSource, fragmentShaderSource);
	mvpMatID = glGetUniformLocation(spriteShader, "mvpMatrix");
	spriteColorID = glGetUniformLocation(spriteShader, "spriteColor");
	
	textures.reserve(MAX_TEXTURES);
}
