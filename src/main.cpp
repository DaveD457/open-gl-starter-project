#include <iostream>
#include <map>
#include <string>

#include "io_manager.h"
#include "resource_loader.h"
#include "shader_compiler.h"
#include "render_manager.h"
#include "audio_manager.h"
#include "physics2d_manager.h"

int main()
{
	IOManager* iomgr = IOManager::getInstance();   
	unsigned int window = iomgr->createWindow("", 100, 100, 500, 500);
	iomgr->setWindowVSync(false);
	if(!window){
		iomgr->displayMessageBox("WINDOW ERROR", "Error creating SDL2 window", 0);
	}

	RenderManager* rdrmgr = RenderManager::getInstance();
	
    rdrmgr->setViewport(0, 0, 500, 500);
    rdrmgr->setClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    
	
	AudioManager* adomgr = AudioManager::getInstance();

	int sampleRate = 10000;
	int totalBytes = 10000;
	unsigned char audioBytes[10000];
	
	for(int i = 0; i < totalBytes; i++){
		audioBytes[i] = i % 256;
	}
	
	for(int i = 0; i < totalBytes; i++){
		audioBytes[i] = ((sin(i) + 2) * 0.5) * 256;
	}
	
	
	unsigned int source = adomgr->createSource();
	unsigned int buffer = adomgr->loadSoundData8M(audioBytes, totalBytes, sampleRate);
	adomgr->attachBufferToSource(buffer, source);
   
	TextRenderer* txtrdr = rdrmgr->getTextRendererInstance();
	txtrdr->setColor(0, 1, 0, 1);
	
	SpriteRenderer* sptrdr = rdrmgr->getSpriteRendererInstance();
	
	int w, h;
	unsigned char* pubDomData = imageToByteArray("../res/images/Public_Domain.png", &w, &h);
	unsigned int pubDomTex = sptrdr->loadImageData(pubDomData, w, h);
	deleteImageData(pubDomData);

	unsigned char checkData[] = {
		0, 0, 0, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 0, 0, 0, 255,
	};
	unsigned int checkTex = sptrdr->loadImageData(checkData, 2, 2);

	Sprite s;
	s.scale = vec2(100, 100);
	s.position = vec2(300, 300);
	s.textureID = checkTex;
	
	AnimatedSprite as;
	as.scale = vec2(100, 100);
	as.position = vec2(100, 400);
	
	sptrdr->addKeyframeToAnimatedSprite(&as, pubDomTex, 1.0f);
	sptrdr->addKeyframeToAnimatedSprite(&as, checkTex, 0.5f);
		
	Physics2DManager* p2dmgr = Physics2DManager::getInstance();
	p2dmgr->setGravity(vec2(0.0f, -1.0f));
	
	unsigned int* pobj = p2dmgr->createPhysicsObject(&as.position, &as.rotation, 1.0f);
	
	long startTime = 0;
	long endTime = 0;
	long totalFrames = 0;
	double runTime = 0;
	float deltaTime = 0;
	float frameTime = 0;
	int frameCount = 0;
	int fps = 0;
	
	float pitch = 1.0;
	
	while(!iomgr->keys[IOManager::ESC_KEY] && !iomgr->QUIT){
		startTime = iomgr->getTime();
        iomgr->processEvents();	
        //////////////////////////////////////////////////////////////////////
	
		if(iomgr->keys[IOManager::SPACE_KEY]){
			p2dmgr->applyForce(*pobj, vec2(0.0f, 0.01f));
		}
		
		p2dmgr->update(deltaTime);
	   	
	   	if(as.position.y < 0){
			as.position.y = 0;
		}
	   	   	   	   
		txtrdr->prepare();
		txtrdr->renderText("TEST", 100, 100, 1);
		txtrdr->renderText(std::to_string(fps).c_str(), 100, 200, 0.5);		
		
		sptrdr->prepare();
		sptrdr->renderSprite(s);
		sptrdr->renderAnimatedSprite(&as, deltaTime);
		if(iomgr->keys[IOManager::Q_KEY]){
			adomgr->playSource(source);
		}
		if(iomgr->keys[IOManager::A_KEY]){
			adomgr->stopSource(source);
		}
		
		if(iomgr->keys[IOManager::UP_KEY]){
			pitch += 0.001f;
			adomgr->setSourcePitch(source, pitch);
		}
		
		if(iomgr->keys[IOManager::DOWN_KEY]){
			pitch -= 0.001f;
			adomgr->setSourcePitch(source, pitch);
		}
			
		///////////////////////////////////////////////////////////////////
	    iomgr->swapWindowBuffer();
	    rdrmgr->clear(GL_COLOR_BUFFER_BIT);
	   
	    frameCount++;
	    totalFrames++;
	    endTime = iomgr->getTime();
	    deltaTime = (float)(endTime - startTime) / 1000.0f;
	    runTime += deltaTime;
		frameTime += deltaTime;
		if(frameTime >= 1.0){
			fps = frameCount;
			frameCount = 0;
			frameTime = 0;
		}
    }

	p2dmgr->shutdown();
	adomgr->shutdown();
	rdrmgr->shutdown();
	iomgr->shutdown();	
	
	return 0;
}
