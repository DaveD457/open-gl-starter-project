#include "physics2d_manager.h"

const int MAX_PHYSICS_OBJECTS = 256;

unsigned int totalPhysicsObjects = 0;
unsigned int totalFreePOHandles = 0;

struct Physics2DManager::PhysicsObject2D{
		vec2* position = nullptr;
		float* rotation = nullptr;
	
		float mass = 1.0f;
		vec2 velocity = vec2(0, 0);
};

Physics2DManager* Physics2DManager::instance = nullptr;
	
Physics2DManager* Physics2DManager::getInstance(){
	if(!instance){
		instance = new Physics2DManager();
	}
		
	return instance;
}
	
Physics2DManager::~Physics2DManager(){

}

void Physics2DManager::shutdown(){
	if(instance){
		delete instance;
		instance = nullptr;
	}
}
	
	
Physics2DManager::Physics2DManager(){
		initialize();
}

void Physics2DManager::initialize(){
	gravity = vec2(0, 0);
	physicsObjects.reserve(MAX_PHYSICS_OBJECTS);
	physObjHandles.reserve(MAX_PHYSICS_OBJECTS);
}

void Physics2DManager::update(float delta){
	for(unsigned int i = 0; i < totalPhysicsObjects; i++){
		physicsObjects[i].velocity += gravity * physicsObjects[i].mass * delta;
		*physicsObjects[i].position += physicsObjects[i].velocity;
	}
}

void Physics2DManager::deletePhysicsObject(unsigned int* id){
	
}

void Physics2DManager::applyForce(unsigned int id, vec2 force){
	physicsObjects[id].velocity += force * physicsObjects[id].mass;
}

unsigned int* Physics2DManager::createPhysicsObject(vec2* position, float* rotation, float mass){
	PhysicsObject2D po;
	po.position = position;
	po.rotation = rotation;
	
	physicsObjects.push_back(po);
	
	if(totalFreePOHandles > 0){
		physObjHandles[freePOHandles.back()] = totalPhysicsObjects;
		freePOHandles.pop_back();
		totalFreePOHandles--;
	}else{
		physObjHandles.push_back(totalPhysicsObjects);
	}
	
	totalPhysicsObjects++;
	
	return &physObjHandles[totalPhysicsObjects - 1];
}

Physics2DManager::PhysicsObject2D* Physics2DManager::getPhysics2DObject(unsigned int id){
	return &physicsObjects[id];
}
