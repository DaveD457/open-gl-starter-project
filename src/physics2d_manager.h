#pragma once

#include <vector>

#include "math_util.h"

class Physics2DManager{
private:
	
	
public:
	struct PhysicsObject2D;

	static Physics2DManager* getInstance();
	
	~Physics2DManager();
	void shutdown();
	void update(float delta);
	void deletePhysicsObject(unsigned int* id);
	void applyForce(unsigned int id, vec2 force);
	unsigned int* createPhysicsObject(vec2* position, float* rotation, float mass);
	PhysicsObject2D* getPhysics2DObject(unsigned int id);
	
	inline void setGravity(vec2 g){ gravity = g; }
private:
	
	static Physics2DManager* instance;
	
	vec2 gravity;
	
	std::vector <PhysicsObject2D> physicsObjects;
	std::vector <unsigned int> physObjHandles;
	std::vector <unsigned int> freePOHandles;
	
	Physics2DManager();
	void initialize();
};
