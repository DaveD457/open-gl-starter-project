#pragma once

#include "sprite_renderer.h"
#include "text_renderer.h"

class RenderManager{
public:
	static RenderManager* getInstance();

	~RenderManager();
	void shutdown();
	void setViewport(int x, int y, int w, int h);
	void setClearColor(float r, float g, float b, float a);
	void clear(int field);
	
	SpriteRenderer* getSpriteRendererInstance();
	TextRenderer* getTextRendererInstance();
	
private:
	static RenderManager* instance;
	
	SpriteRenderer* sptrdr;
	TextRenderer* txtrdr;
	
	RenderManager();
	void initialize();
};
