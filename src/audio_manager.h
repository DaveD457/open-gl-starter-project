#pragma once

class AudioManager{
public:
	static AudioManager* getInstance();

	~AudioManager();
	void shutdown();
	void playSource(unsigned int source);
	void stopSource(unsigned int source);
	void deleteSource(unsigned int source);
	void attachBufferToSource(unsigned int buffer, unsigned int source);
	void deleteBuffer(unsigned int buffer);
	void setSourcePitch(unsigned int source, float pitch);
	unsigned int createSource();
	unsigned int loadSoundData8M(unsigned char* data, unsigned int sizeInBytes, unsigned int sampleFreq);
	
private:
	static AudioManager* instance;
	
	AudioManager();
	void initialize();
};
